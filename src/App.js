import React, {useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

import Layout from './Components/Layout';
import Login from './Components/Login';
import ProtectedRouter from './ProtectedRouter';


import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import Detail from './Components/Detail';
import Navbar from './Components/Navbar';
import Edit from './Components/Edit';
import Create from './Components/Create';



function App() {

  return ( 

    
    <Router>
      <Navbar/>
      <Switch>
              

        <ProtectedRouter exact path="/" component={Layout} />
        <ProtectedRouter exact path="/create" component={Create} />
        <ProtectedRouter exact path="/detail/:id" component={Detail} />
        <ProtectedRouter exact path="/edit/:id" component={Edit} />

        {/* <Route exact path="/login" component={Login} /> */}
        <Route
        path="/login"
        component={Login}
        />

          

      </Switch>
    </Router>
  );
}

export default App;
