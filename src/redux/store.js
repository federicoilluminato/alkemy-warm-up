import {configureStore} from '@reduxjs/toolkit';
import PostsReducer from './slice';

export default configureStore({
    reducer: {
        posts: PostsReducer,
    }
})