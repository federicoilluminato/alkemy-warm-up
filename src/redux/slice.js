import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getPostsAsync = createAsyncThunk('posts/getPostsAsync',
async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    if(response.ok){
        const posts = await response.json();
        return { posts }
    }
})

export const addPostAsync = createAsyncThunk('posts/addPostAsync', async(payload) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
        },
        body: JSON.stringify({title : payload.title, text: payload.body})
    })
    if(response.ok){
        const post = await response.json();
        alert('posted!')
        return { post };
    }
})

export const findPostAsync = createAsyncThunk('posts/findPostAsync',
async(id)=> {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
    if(response.ok){
        const post = await response.json();
        return { post } 
    }
})

export const updatePostAsync = createAsyncThunk('posts/updatePostAsync', 
async(id, payload) => {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: 'PATCH',
        headers: {
            'Content-Type':'application/json',
        },
        body: JSON.stringify({title: payload.title, text: payload.body})
    });
    if(response.ok){
        const update = await response.json();
        return { update }
    }
})


const slice = createSlice({
    name: 'posts',
    initialState: [

    ],
    reducers:{

        addPost: (state, action) => {
            const newPost = {
                id: Date.now(),
                title: action.payload.title,
                text: action.payload.body
            };
            state.push(newPost);
        },
    },
    extraReducers: {
        [getPostsAsync.fulfilled] : (state, action) => {
            return action.payload.posts
        },
        [addPostAsync.fulfilled]: (state,action) => {
            state.push(action.payload.post);
        },
        [findPostAsync.fulfilled]: (state,action) => {
            return action.payload.post
        },
        [updatePostAsync.fulfilled]: (state,action) => {
            return action.payload.update
        },
    }
})

export const { getPosts, addPost } = slice.actions;


export default slice.reducer;