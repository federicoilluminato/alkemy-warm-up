import React from 'react';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';

import { addPostAsync } from '../redux/slice';
import { useDispatch } from 'react-redux';

import { MDBBtn, MDBInput } from 'mdb-react-ui-kit';

const Create = () => {

    const dispatch = useDispatch();

    const formik = useFormik({
        initialValues: {
            title: '',
            text: '',
        },
        onSubmit: values => {
            console.log(values)

            if(!values.title && !values.text){
                alert('Titulo & Texto requerido')
            }else if(!values.title){
                alert(`Titulo requerido`)
            }else if(!values.text){
                alert(`Texto requerido`)
            }else{
                dispatch(addPostAsync({
                    title : formik.values.title,
                    body: formik.values.text
                }))            
            }
        }
    })
    



    return (
        <div>
            <h1>Form de create</h1>

            <div
            className="p-3"
            >
            <form
            className="p-3"
            onSubmit={formik.handleSubmit}
            >
            <MDBInput
            className="my-4" 
            label='Post Title' 
            id='typeText'
            name='title' 
            type='text' 
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            />

            <MDBInput
            className="my-4"  
            label='Post Text' 
            id='textAreaExample'
            name='text'
            textarea rows={4} 
            value={formik.values.text}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            />


            <MDBBtn type="submit">Confirmar</MDBBtn>

            </form>
            </div>


        </div>
    )
}

export default Create
