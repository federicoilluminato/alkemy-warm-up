import React, {useEffect,useState} from 'react';

import { useFormik } from 'formik';
import { useHistory, useParams } from 'react-router-dom';

import { MDBBtn, MDBInput } from 'mdb-react-ui-kit';
import { useDispatch} from 'react-redux';
import { updatePostAsync} from '../redux/slice'

const EditForm = ({title, body, id}) => {

    const dispatch = useDispatch();

    const formik = useFormik({
        initialValues: {
            title: `${title}`,
            text: `${body}`,
        },
        onSubmit: values => {

            if(!values.title && !values.text){
                alert('Titulo & Texto requerido')
            }else if(!values.title){
                alert(`Titulo requerido`)
            }else if(!values.text){
                alert(`Texto requerido`)
            }else{
                dispatch(updatePostAsync(id))
                .then((res) => {
                    console.log(res)
                }).catch((err)=>alert(err))
        }}
    })



    return (
        <div>

            <h1>Formulario de edición</h1>

            <div
            className="p-3"
            >
            <form
            className="p-3"
            onSubmit={formik.handleSubmit}
            >
            <MDBInput
            className="my-4" 
            label='Post Title' 
            id='typeText'
            name='title' 
            type='text' 
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            />

            <MDBInput
            className="my-4"  
            label='Post Text' 
            id='textAreaExample'
            name='text'
            textarea rows={4} 
            value={formik.values.text}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            />


            <MDBBtn type="submit">Confirmar</MDBBtn>

            </form>
            </div>
            
        </div>
    )
}

export default EditForm
