import React, {useEffect, useState} from 'react';
import { MDBContainer } from 'mdb-react-ui-kit';
import { useHistory, useParams } from 'react-router-dom'

const Detail = () => {

    const [details, setDetails] = useState();
    const { id } = useParams();

    const getDetail = async () => {
        const data = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
        const response = await data.json();
        setDetails(response)
      }


    useEffect(() => {
        getDetail();
    },[])

    return (
        <MDBContainer fluid>
            <div className="p-5">
            {details ? 
                <div>
                <h1>{details.title}</h1>


                <p>{details.body}</p>
            
        
                </div>
            : 
                null
                // alert('error!')
            
            
            }
            </div>
        </MDBContainer>
    )
}

export default Detail
