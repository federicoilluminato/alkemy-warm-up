import React, {useEffect, useState} from 'react';
import { useFormik } from 'formik';
import { Redirect, useHistory } from 'react-router-dom';

import { MDBInput } from 'mdb-react-ui-kit';
import { MDBContainer } from 'mdb-react-ui-kit';
import { MDBBtn } from 'mdb-react-ui-kit';

const Login = () => {

    
    const history = useHistory();

    const formik = useFormik({
        initialValues: {
            email:'',
            password:''
        },
        onSubmit: (values) => {
            console.log(values);
            if(!values.email && !values.password){
                alert('Email & Contraseña requerido')
            }
            else if(!values.email){
                alert(`Email requerido`)
            }else if(!values.password){
                alert(`Contraseña requerida`)
            }else{
                fetch('http://challenge-react.alkemy.org/', {
                    method: 'POST',
                    headers: {"Content-Type": "application/json" },
                    body: JSON.stringify(values)
                }).then((res) => {
                    console.log(res)
                    if(res.ok === false){
                        alert(res.statusText);
                    }else{
                        localStorage.setItem('token', 'logged')
                        history.push('/')
                    }
                })
                .catch((err) => {
                    alert(err)
                })
            }
            
        },
    });





    return (
        <MDBContainer>
            <div className="d-flex align-items-center justify-content-center mt-3 mb-3 text-center">
                <form 
                className=""
                onSubmit={formik.handleSubmit}
                >
                    {/* <label htmlFor="email">Email</label> */}
                    <MDBInput
                        className="my-3"
                        label='email'
                        id="email"
                        name="email"
                        type="email"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.email}
                    />

                    {/* <label htmlFor="password">Password</label> */}
                    <MDBInput
                        className="my-3"
                        label="contraseña"
                        id="password"
                        name="password"
                        type="password"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.password}
                    />

                    <MDBBtn type="submit">Enviar</MDBBtn>

                </form>
            </div>
        </MDBContainer>
    )
}

export default Login
