import React from 'react';
import { MDBContainer } from 'mdb-react-ui-kit';
import { MDBCard, MDBCardBody } from 'mdb-react-ui-kit';
import { MDBIcon } from 'mdb-react-ui-kit';

import {useSelector} from 'react-redux';


const Post = ({title,id}) => {

    const posts = useSelector((state) => state.posts);

    const deleteFnc = () => {

        fetch(` https://jsonplaceholder.typicode.com/posts/${id}`,{
            method:'DELETE',
        }).then((res)=> {
            alert(`eliminado post ${id} ${res.ok}`)
            console.log(res)
        }).catch((err)=> {
            alert(err)
        })
    }


    return (
        <MDBContainer>

        <MDBCard className="my-3 hover-shadow quare border border-dark">
            <MDBCardBody className="d-flex">
            <h1 className="flex-grow-1 px-3 w-100">{title}</h1>

                <div className="d-flex button-style justify-content-evenly">
                <a href={`detail/${id}`} className=""><MDBIcon fas icon="chevron-circle-down" /></a>
                <a href={`edit/${id}`} className=""><MDBIcon fas icon="pen" /></a>
                <a className="" onClick={()=>{deleteFnc(id)}}><MDBIcon fas icon="trash-alt" className="pointer" /></a>
                </div>

            </MDBCardBody>
        </MDBCard>
        
        
        
        </MDBContainer>
    )
}

export default Post
