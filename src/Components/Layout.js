import React, { useState, useEffect } from 'react';
import Home from './Home';


import { getPostsAsync} from '../redux/slice'
import { useDispatch, useSelector } from 'react-redux';


const Layout = () => {

    const dispatch = useDispatch();
    const postsList = useSelector((state) => state.posts)

    
    useEffect(() => {
        dispatch(getPostsAsync())
    },[dispatch])
   

    return (
        <div>
            <Home posts={postsList} />
        </div>
    )
}

export default Layout
