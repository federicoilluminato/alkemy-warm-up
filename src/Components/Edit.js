import React, {useEffect,useState} from 'react';
import { useFormik } from 'formik';
import { useHistory, useParams } from 'react-router-dom';

import { MDBBtn, MDBInput } from 'mdb-react-ui-kit';

import { findPostAsync } from '../redux/slice'
import { useDispatch, useSelector } from 'react-redux';
import EditForm from './EditForm';

const Edit = () => {

    const [details, setDetails] = useState();
    const { id } = useParams();


    const dispatch = useDispatch();
    const postsList = useSelector((state) => state.posts)
    

    
    useEffect(() => {
        dispatch(findPostAsync(id))
        setDetails(postsList)
    },[dispatch]) 
    
    
    return (
        <div>
            
            {postsList.title ? 
            
            <EditForm 
            title={postsList.title}
            body={postsList.body}
            id={id}
        
            /> 
            
            : null}



        </div>
    )
}

export default Edit
