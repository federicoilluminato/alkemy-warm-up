import React from 'react';
import { MDBContainer } from 'mdb-react-ui-kit';
import Post from './Post';

const Home = ({posts}) => {

    console.log(posts)


    return (
        <MDBContainer fluid>
            
            <div className="home-posts mt-5">

                <div>
                    {posts ? posts.map((post) => {
                        return (<Post
                            title={post.title}
                            id={post.id}
                            body={post.body}
                        />)
                    
                        

                    }) : null}
                    {/* <Post /> */}
                </div>


            </div>
        </MDBContainer>
    )
}

export default Home
