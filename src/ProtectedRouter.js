import React from 'react';
import { Route, Redirect } from 'react-router-dom';



const ProtectedRouter = ({component: Component, ...rest}) => {
    
    
    
    const isAuth = localStorage.getItem('token');
    
    return (
        <Route {...rest}>{isAuth ? <Component /> : <Redirect to="/login" />}</Route>
            
        
    )
}

export default ProtectedRouter
